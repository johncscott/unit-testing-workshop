﻿using NUnit.Framework;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise1
{
    [TestFixture]
    public class Exercise1Tests
    {
        ForumMemberService _forumMemberService;

        [SetUp]
        public void Setup()
        {
            _forumMemberService = new ForumMemberService();
        }



        [Test]
        public void Create_Member_Success_Because_Member_Does_Not_Exist()
        {
            var result = _forumMemberService.Create("SomeName", "name@company.com", "password");

            Assert.AreEqual("success", result);
        }


        [Test]
        public void Create_Member_Fails_Because_Member_Exists()
        {
            // TODO: finish me!

            //var result = _forumMemberService.Create("SomeName", "name@company.com", "password");

            //Assert.AreEqual("exists", result);
        }
    }
}
