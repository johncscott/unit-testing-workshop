﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public interface IPostService
    {
        string CreateTopic(string nameCompanyCom, string topicBody, int parentNodeId, int i);
    }


    public class PostService : IPostService
    {
        private readonly IContentService _contentService;

        public PostService(IContentService contentService)
        {
            _contentService = contentService;
        }



        /// <summary>
        /// Creates forum topic.
        /// </summary>
        public string CreateTopic(string topicTitle, string topicBody, int parentNodeId, int memberId)
        {
            //if (string.IsNullOrWhiteSpace(topicTitle))
            //{
            //    return "invalid title";
            //}

            var post = _contentService.CreateContent(topicTitle, parentNodeId, "Post");
        
            post.SetValue("memberId", memberId);
            _contentService.Save(post);
            _contentService.Publish(post);

            return "success";
        }

    }
}
